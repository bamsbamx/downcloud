package com.iamaner.downcloud;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.iamaner.downcloud.entities.Track;
import com.iamaner.downcloud.fragment.FragmentLoggedOut;
import com.iamaner.downcloud.fragment.FragmentStream;
import com.iamaner.downcloud.fragment.FragmentTrack;
import com.iamaner.downcloud.util.FileUtils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.soundcloud.api.ApiWrapper;

import java.util.List;


public class ActivityMain extends BaseActivity implements SlidingUpPanelLayout.PanelSlideListener,
        FragmentStream.OnTrackSelectedListener{

    private FragmentLoggedOut mFragmentLoggedOut;
    private FragmentStream mFragmentStream;
    private FragmentTrack mFragmentTrack;

    private SlidingUpPanelLayout mSlidingLayout;

    private ApiWrapper mApiWrapper;

    private ServicePlayer3 mServicePlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setIcon(R.drawable.ic_drawer);

        mSlidingLayout = (SlidingUpPanelLayout) findViewById(R.id.activity_main_sliding_layout);
        mSlidingLayout.setPanelSlideListener(this);

        mApiWrapper = FileUtils.getApiWrapper(this);
        if (savedInstanceState == null) {
            if (mApiWrapper == null) {
                mFragmentLoggedOut = FragmentLoggedOut.newInstance();
                getFragmentManager().beginTransaction()
                        .add(R.id.activity_main_fragment_container, mFragmentLoggedOut)
                        .commit();
            }else {
                mFragmentTrack = (FragmentTrack) getFragmentManager().findFragmentById(R.id.activity_main_fragment_track);
                mFragmentStream = FragmentStream.newInstance();
                getFragmentManager().beginTransaction()
                        .add(R.id.activity_main_fragment_container, new FragmentStream())
                        .commit();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
//        if(playIntent==null){
            Intent playIntent = new Intent(this, ServicePlayer3.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(musicConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mSlidingLayout.isPanelExpanded()) mSlidingLayout.collapsePanel();
                return true;
            case R.id.action_search:
                Intent intent = new Intent(this, ActivitySearch.class);
                startActivity(intent);
//                onSearchRequested();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mSlidingLayout.isPanelExpanded()) mSlidingLayout.collapsePanel();
        else super.onBackPressed();
    }

    public void onLoggedIn() {
        ((FragmentLoggedOut.DialogLogin) getFragmentManager().findFragmentByTag("dialog")).dismiss();
        mApiWrapper = FileUtils.getApiWrapper(this);
        mFragmentTrack = (FragmentTrack) getFragmentManager().findFragmentById(R.id.activity_main_fragment_track);
        mFragmentStream = FragmentStream.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.activity_main_fragment_container, mFragmentStream)
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .commit();
        mFragmentLoggedOut = null;
    }

    public ApiWrapper getApiWrapper() {
        //mApiWrapper can be null if GC has worked
        if(mApiWrapper == null) return FileUtils.getApiWrapper(this);
        return mApiWrapper;
    }

    public void togglePlay() {
        mServicePlayer.toggle();
    }


    @Override
    public void onTrackSelected(List<Track> mTrackList, int position) {
        mFragmentTrack.setTrack(mTrackList.get(position));
        mSlidingLayout.expandPanel();
        mServicePlayer.setPlayList(mTrackList, position);
    }

    @Override
    public void onPanelSlide(View view, float v) {
        mFragmentTrack.setHeaderAlpha(1-v);
//        mSlidingLayout.findViewById(R.id.activity_main_sliding_layout_header).setAlpha(1-v);
        int accentColor = getResources().getColor(R.color.accent_color);
        getActionBar().setBackgroundDrawable(new ColorDrawable(
                Color.argb(Math.round((1-v)*255),
                Color.red(accentColor),
                Color.green(accentColor),
                Color.blue(accentColor))));
    }

    @Override
    public void onPanelCollapsed(View view) {
        getActionBar().setIcon(R.drawable.ic_drawer);
    }

    @Override
    public void onPanelExpanded(View view) {
        getActionBar().setIcon(R.drawable.ic_action_up);
    }

    @Override
    public void onPanelAnchored(View view) { }

    @Override
    public void onPanelHidden(View view) { }

    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicePlayer3.PlayerBinder binder = (ServicePlayer3.PlayerBinder) service;
            //get service
            mServicePlayer = binder.getService();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServicePlayer = null;
        }
    };
}
