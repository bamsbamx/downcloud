package com.iamaner.downcloud.entities;

import org.json.JSONException;
import org.json.JSONObject;

public class User extends JSONObject {

    public User(JSONObject user) throws JSONException {
        super(user.toString());
    }

    public int getId() {
        try {
            return getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public String getUserName() {
        try {
            return getString("username");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getAvatarUrl() {
        try {
            return getString("avatar_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
