package com.iamaner.downcloud.entities;


import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Http;
import com.soundcloud.api.Request;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Track extends JSONObject{

    private User mUser;


    public Track(JSONObject track) throws JSONException {
        super(track.toString());
        this.mUser = new User(getJSONObject("user"));
    }

    //DO IN BACKGROUND
    public static List<Track> getStreamTracks(ApiWrapper apiWrapper, int limit) throws IOException, JSONException {
        List<Track> trackList = new ArrayList<Track>();
        Request request = Request.to("/me/activities");
        request.add("limit", limit);
        request.add("oauth_token", apiWrapper.getToken());
        HttpResponse response = apiWrapper.get(request);
        JSONObject jsonObject = new JSONObject(Http.formatJSON(Http.getString(response)));
        JSONArray collections = jsonObject.getJSONArray("collection");
        int jObjectCount = collections.length();
        for (int i = 0; i < jObjectCount; i++) {
            JSONObject item = collections.getJSONObject(i);
            if (item.getString("type").equals("track")) {
                trackList.add(new Track(item.getJSONObject("origin")));
            }
        }
        return trackList;
    }

    public int getId() {
        try {
            return getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public String getCreatedAt() {
        try {
            Date createdAt = parseDate(getString("created_at"));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(createdAt);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d MMMM y");
            return simpleDateFormat.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getTitle() {
        try {
            return getString("title");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getArtworkUrl() {
        // artwork_url will be null if the user does not upload an artwork, so try to catch the user
        // avatar if an exception is thrown
        try {
            String url = getString("artwork_url");
            return url.equals("null") ? getAuthorAvatarUrl() : url;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getLargeArtworkUrl() {
        // artwork_url will be null if the user does not upload an artwork, so try to catch the user
        // avatar if an exception is thrown
        try {
            String url = getString("artwork_url");
            url = url.equals("null") ? getAuthorAvatarUrl() : url;
            url = url.replace("large", "t500x500");
            return url;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getWaveformUrl() {
        try {
            return getString("waveform_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getAuthorUsername() {
        return mUser.getUserName();
    }

    public String getAuthorAvatarUrl() {
        return mUser.getAvatarUrl();
    }

    public String getTrackStreamUrl(String oauthToken) {
        try {
            return getString("stream_url") + "?oauth_token=" +oauthToken;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private Date parseDate(String date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z");
        return dateFormat.parse(date);
    }


}
