package com.iamaner.downcloud;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.iamaner.downcloud.util.RecentSearchSuggestionProvider;

public class ActivitySearch extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                onSearchRequested();
                return true;
            default:
                return false;
        }
    }



    private void handleIntent(Intent intent) {
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY); // from search-bar
            saveRecentSearch(query);
//            Toast.makeText(getApplicationContext(), "Start ActivityFriend to show " + queryString2, Toast.LENGTH_SHORT).show();
        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
//            showResults(query);
            saveRecentSearch(query);
        }
    }

    private void saveRecentSearch(String query) {
        SearchRecentSuggestions suggestions =
                new SearchRecentSuggestions(this,
                        RecentSearchSuggestionProvider.AUTHORITY,
                        RecentSearchSuggestionProvider.MODE);
        suggestions.saveRecentQuery(query, null);
    }
}
