package com.iamaner.downcloud.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iamaner.downcloud.Downcloud;
import com.iamaner.downcloud.R;
import com.iamaner.downcloud.entities.Track;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ArrayAdapterStream extends BaseAdapter {

    private final Activity mActivity;
    private List<Track> mTrackList;

    public ArrayAdapterStream(Activity activity, List<Track> trackList) {
        this.mActivity = activity;
        this.mTrackList = trackList;
    }

    @Override
    public int getCount() {
        return mTrackList.size();
    }

    @Override
    public Track getItem(int i) {
        return mTrackList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Track track = getItem(position);

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.listitem_stream_track, parent, false);
            holder = new ViewHolder();
            holder.artwork = (ImageView) convertView.findViewById(R.id.listitem_stream_track_imageview_artwork);
            holder.title = (TextView) convertView.findViewById(R.id.listitem_stream_track_textview_title);
            holder.author = (TextView) convertView.findViewById(R.id.listitem_stream_track_textview_author);
            convertView.setTag(holder);
        }
        else holder = (ViewHolder) convertView.getTag();

//        holder.artwork.setImageUrl(track.getArtworkUrl(), mImageLoader);
        Picasso.with(mActivity).load(track.getArtworkUrl()).into(holder.artwork);
        holder.title.setText(track.getTitle());
        holder.author.setText(track.getAuthorUsername());

        return convertView;
    }

    static class ViewHolder {
        ImageView artwork;
        TextView title;
        TextView author;
    }
}
