package com.iamaner.downcloud;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.iamaner.downcloud.entities.Track;
import com.iamaner.downcloud.util.FileUtils;

import java.io.IOException;
import java.util.List;

public class ServicePlayer3 extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    public static final String ACTION_TOGGLE = "com.iamaner.downcloud.serviceplayer.action.TOGGLE_PLAYBACK";
    public static final String ACTION_PLAY = "com.iamaner.downcloud.serviceplayer.action.PLAY";
    public static final String ACTION_PAUSE = "com.iamaner.downcloud.serviceplayer.action.PAUSE";
    public static final String ACTION_STOP = "com.iamaner.downcloud.serviceplayer.action.STOP";
    public static final String ACTION_SKIP = "com.iamaner.downcloud.serviceplayer.action.SKIP";
    public static final String ACTION_REWIND = "com.iamaner.downcloud.serviceplayer.action.REWIND";
    private static final int NOTIFICATION_ID = 4756;

    private IBinder mPlayerBinder = new PlayerBinder();

    private WifiManager.WifiLock mWifiLock;
    private MediaPlayer mMediaPlayer;
    private Notification mNotification;

    private String mOauthToken;
    private List<Track> mPlayList;
    private int mTrackPosition;

    @Override
    public void onCreate() {
        super.onCreate();
        mOauthToken = FileUtils.getApiWrapper(this).getToken().access;
        mWifiLock = ((WifiManager) getSystemService(WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "downcloud_wifilock");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mPlayerBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action != null) {
            if (action.equals(ACTION_TOGGLE)) toggle();
            else if (action.equals(ACTION_PLAY)) play();
            else if (action.equals(ACTION_REWIND)) rewind();
            else if (action.equals(ACTION_SKIP)) skip();
        }
        return START_NOT_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        //stop & release mediaplayer
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWifiLock.isHeld()) mWifiLock.release();
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mMediaPlayer.setVolume(1.0f, 1.0f);
        if (!mMediaPlayer.isPlaying()) mMediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        skip();
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        Toast.makeText(getApplicationContext(), "Media player error! Resetting.",
                Toast.LENGTH_SHORT).show();
        Log.e("TAG", "Error: what=" + String.valueOf(what) + ", extra=" + String.valueOf(extra));
        return true;
    }

    public class PlayerBinder extends Binder {
        ServicePlayer3 getService() {
            return ServicePlayer3.this;
        }
    }

    public void setPlayList(List<Track> playList, int startFrom) {
        this.mPlayList = playList;
        this.mTrackPosition = startFrom;
        play();
    }

    public void toggle() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying()){
                mMediaPlayer.pause();
                if (mWifiLock.isHeld()) mWifiLock.release();
            }
            else onPrepared(mMediaPlayer);
        }
    }

    private void play() {
        if (mMediaPlayer == null) {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.setOnErrorListener(this);
        }else {
            mMediaPlayer.reset();
        }
        try {
            setUpAsForeground("text");
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setDataSource(mPlayList.get(mTrackPosition).getTrackStreamUrl(mOauthToken));
            mMediaPlayer.prepareAsync();
            mWifiLock.acquire();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void rewind() {
        mMediaPlayer.seekTo(0);
    }

    private void skip() {
        mTrackPosition++;
        if (mWifiLock.isHeld()) mWifiLock.release();
        play();
    }


    //TODO: use notification.Builder
    private void setUpAsForeground(String text) {
        PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0,
                new Intent(getApplicationContext(), ActivityMain.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        mNotification = new Notification();
        mNotification.tickerText = text;
        mNotification.icon = R.drawable.ic_drawer;
        mNotification.flags |= Notification.FLAG_ONGOING_EVENT;
        mNotification.setLatestEventInfo(getApplicationContext(), "RandomMusicPlayer",
                text, pi);
        startForeground(NOTIFICATION_ID, mNotification);
    }
}
