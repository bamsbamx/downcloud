package com.iamaner.downcloud.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.iamaner.downcloud.ActivityMain;
import com.iamaner.downcloud.R;
import com.iamaner.downcloud.util.FileUtils;
import com.iamaner.downcloud.view.LoginButton;
import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.CloudAPI;
import com.soundcloud.api.Token;

import java.io.IOException;
import java.net.UnknownHostException;

public class FragmentLoggedOut extends DialogFragment {

    private ActivityMain mActivity;

    public static FragmentLoggedOut newInstance() {
        return new FragmentLoggedOut();
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = (ActivityMain) activity;
        mActivity.getActionBar().hide();
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_logged_out, container, false);
        LoginButton loginButton = (LoginButton) rootView.findViewById(R.id.fragment_logged_out_login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                FragmentLoggedOut.DialogLogin dialogLogin = new FragmentLoggedOut.DialogLogin();
                dialogLogin.show(fm, "dialog");
            }
        });
        return rootView;
    }

    @Override
    public void onDetach() {
        getActivity().getActionBar().show();
        super.onDetach();
    }


    public static class DialogLogin extends DialogFragment {

        private EditText mUserName, mPassword;
        private Button mLoginButton;

        public DialogLogin() {
            setStyle(STYLE_NO_TITLE, 0);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragmentdialog_login, container);
            mUserName = (EditText) view.findViewById(R.id.fragmentdialog_login_edittext_username);
            mPassword = (EditText) view.findViewById(R.id.fragmentdialog_login_edittext_password);
            mLoginButton = (Button) view.findViewById(R.id.fragmentdialog_login_button_login);
            mLoginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new LoginTask((ActivityMain) getActivity(), mUserName.getText().toString(), mPassword.getText().toString()).execute();
                }
            });
            return view;
        }
    }


    private static class LoginTask extends AsyncTask<Void, Void, Boolean> {

        private final ActivityMain mActivity;
        private final String mUsername;
        private final String mPassword;

        private boolean mSucceded;
        private String mErrorCause;
        private ProgressDialog mProgressDialog;

        public LoginTask(ActivityMain activity, String username, String password) {
            this.mUsername = username;
            this.mPassword = password;
            this.mActivity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSucceded = true;
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setMessage(mActivity.getString(R.string.text_logging_in));
            mProgressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            //Define your CLIENT_ID and CLIENT_SECRET from your Souncloud app in your strings.xml file
            ApiWrapper wrapper = new ApiWrapper(
                    mActivity.getString(R.string.CLIENT_ID),
                    mActivity.getString(R.string.CLIENT_SECRET),
                    null,
                    null);
            try {
                wrapper.login(mUsername, mPassword, Token.SCOPE_NON_EXPIRING);
                wrapper.toFile(FileUtils.getApiWrapperFile(mActivity));
            } catch (IOException e) {
                if (e instanceof UnknownHostException)
                    mErrorCause = mActivity.getString(R.string.error_disconnected_internet);
                else if (e instanceof CloudAPI.InvalidTokenException)
                    mErrorCause = mActivity.getString(R.string.error_invalid_token);
                mSucceded = false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            mProgressDialog.dismiss();
            if (!mSucceded) {
                Toast.makeText(mActivity, mErrorCause, Toast.LENGTH_SHORT).show();
                return;
            }
            mActivity.onLoggedIn();
        }
    }

}
