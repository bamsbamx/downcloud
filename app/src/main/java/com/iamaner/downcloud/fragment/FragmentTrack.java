package com.iamaner.downcloud.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.iamaner.downcloud.ActivityMain;
import com.iamaner.downcloud.R;
import com.iamaner.downcloud.entities.Track;
import com.soundcloud.api.ApiWrapper;
import com.squareup.picasso.Picasso;

public class FragmentTrack extends Fragment {

    private ActivityMain mActivity;
    private Track mTrack;
    private ApiWrapper mApiWrapper;

    private View mHeaderContainer;
    private ImageView mHeaderImage, mArtworkImage, mWaveformImage;
    private TextView mHeaderTitle;
    private ImageButton mTogglePlay;

    public static FragmentTrack newInstance() {
        return new FragmentTrack();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActivityMain) getActivity();
        mApiWrapper = mActivity.getApiWrapper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_track, container, false);
        mHeaderContainer = v.findViewById(R.id.fragment_track_sliding_layout_header);
        mHeaderImage = (ImageView) v.findViewById(R.id.fragment_track_header_image);
        mArtworkImage = (ImageView) v.findViewById(R.id.fragment_track_imageview_artwork);
        mWaveformImage = (ImageView) v.findViewById(R.id.fragment_track_imageview_waveform);
        mHeaderTitle = (TextView) v.findViewById(R.id.fragment_track_header_title);

        mTogglePlay = (ImageButton) v.findViewById(R.id.fragment_track_header_toggleplay);
        mTogglePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.togglePlay();
            }
        });
//
//        mTitleText.setText(mTrack.getTitle());
//        mAuthorText.setText(mTrack.getAuthorUsername());
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void setTrack(Track track) {
        this.mTrack = track;

        mHeaderTitle.setText(mTrack.getTitle());
        Picasso picasso = Picasso.with(mActivity);
        Log.wtf("TAG", mTrack.getLargeArtworkUrl());
        picasso.load(mTrack.getArtworkUrl()).into(mHeaderImage);
        picasso.load(mTrack.getLargeArtworkUrl()).into(mArtworkImage);
        picasso.load(mTrack.getWaveformUrl()).into(mWaveformImage);
    }

    public void setHeaderAlpha(float alpha) {
        mHeaderContainer.setAlpha(alpha);
    }

}
