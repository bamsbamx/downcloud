package com.iamaner.downcloud.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.iamaner.downcloud.ActivityMain;
import com.iamaner.downcloud.R;
import com.iamaner.downcloud.adapter.ArrayAdapterStream;
import com.iamaner.downcloud.entities.Track;
import com.soundcloud.api.ApiWrapper;
import com.soundcloud.api.Http;
import com.soundcloud.api.Request;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FragmentStream extends Fragment {

    private ActivityMain mActivity;
    private ApiWrapper mApiWrapper;

    private ListView mListView;
    private List<Track> mTrackList = new ArrayList<Track>();
    private OnTrackSelectedListener mOnTrackSelectedListener;

    public static FragmentStream newInstance() {
        return new FragmentStream();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnTrackSelectedListener = (OnTrackSelectedListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ActivityMain) getActivity();
        mApiWrapper = mActivity.getApiWrapper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stream, container, false);
        mListView = (ListView) v.findViewById(R.id.fragment_stream_listview);
        View mEmptyView = v.findViewById(R.id.fragment_stream_empty_list);
        mListView.setEmptyView(mEmptyView);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new StreamLoadTask(mActivity, mApiWrapper, mListView).execute();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mOnTrackSelectedListener != null)
                    mOnTrackSelectedListener.onTrackSelected(mTrackList, i);
            }
        });
    }


    public interface OnTrackSelectedListener {
        void onTrackSelected(List<Track> track, int position);
    }



    private class StreamLoadTask extends AsyncTask<Void, Void, Void> {

        private final Activity mActivity;
        private final ApiWrapper mApiWrapper;
        private ListView mListView;

        public StreamLoadTask(Activity activity, ApiWrapper apiWrapper, ListView listView) {
            this.mActivity = activity;
            this.mApiWrapper = apiWrapper;
            this.mListView = listView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                mTrackList = Track.getStreamTracks(mApiWrapper, 30);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mListView.setAdapter(new ArrayAdapterStream(mActivity, mTrackList));
        }
    }
}
