package com.iamaner.downcloud.util;

import android.content.Context;

import com.soundcloud.api.ApiWrapper;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    public static File getApiWrapperFile(Context context) {
        return new File(context.getFilesDir(), "wrapper.ser");
    }

    public static ApiWrapper getApiWrapper(Context context) {
        try {
            return ApiWrapper.fromFile(getApiWrapperFile(context));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
