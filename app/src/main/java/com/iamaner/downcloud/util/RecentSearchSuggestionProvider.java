package com.iamaner.downcloud.util;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SearchRecentSuggestionsProvider;
import android.database.Cursor;
import android.net.Uri;

public class RecentSearchSuggestionProvider extends SearchRecentSuggestionsProvider {

    public static final String AUTHORITY = RecentSearchSuggestionProvider.class.getName();

    public static final int MODE = DATABASE_MODE_QUERIES;

    public RecentSearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }

    @Override
    public boolean onCreate() {
        return super.onCreate();
    }

    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings2, String s2) {
        return super.query(uri, strings, s, strings2, s2);
    }

    @Override
    public String getType(Uri uri) {
        return super.getType(uri);
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return super.insert(uri, contentValues);
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return super.delete(uri, s, strings);
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return super.update(uri, contentValues, s, strings);
    }
}
