package com.iamaner.downcloud;

import android.app.Application;

public class Downcloud extends Application {

    public static final String TAG = Downcloud.class.getSimpleName();

    private static Downcloud mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized Downcloud getInstance() {
        return mInstance;
    }

}
