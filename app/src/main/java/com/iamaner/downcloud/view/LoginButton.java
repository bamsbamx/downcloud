package com.iamaner.downcloud.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.Button;

import com.iamaner.downcloud.R;

public class LoginButton extends Button {

    public LoginButton(Context context) {
        super(context);
        finishInit();
    }

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (attrs.getStyleAttribute() == 0) {
            this.setGravity(Gravity.CENTER);
            this.setTextColor(getResources().getColor(android.R.color.white));
            this.setTypeface(Typeface.DEFAULT_BOLD);
            this.setBackgroundResource(R.drawable.btn_login);
            this.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_launcher, 0, 0, 0);
            this.setCompoundDrawablePadding(
                    getResources().getDimensionPixelSize(R.dimen.login_button_compound_padding));
            this.setPadding(getResources().getDimensionPixelSize(R.dimen.login_button_padding),
                    getResources().getDimensionPixelSize(R.dimen.login_button_padding),
                    getResources().getDimensionPixelSize(R.dimen.login_button_padding),
                    getResources().getDimensionPixelSize(R.dimen.login_button_padding));

        }
//        parseAttributes(attrs);
    }

    public LoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
//        parseAttributes(attrs);
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        finishInit();
    }

    private void finishInit() {
        setButtonText();

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setButtonText();
    }

//    private void parseAttributes(AttributeSet attrs) {
//        TypedArray a = getContext().obtainStyledAttributes(attrs, com.facebook.android.R.styleable.com_facebook_login_view);
//        a.recycle();
//    }

    private void setButtonText() {
        setText(getResources().getString(R.string.text_login_button));

    }



}
