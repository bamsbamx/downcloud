package com.iamaner.downcloud;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

import com.iamaner.downcloud.util.SystemBarTintManager;

public class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SystemBarTintManager systemBarTintManager = new SystemBarTintManager(this);
        systemBarTintManager.setStatusBarTintEnabled(true);
        systemBarTintManager.setStatusBarTintColor(getResources().getColor(R.color.accent_color));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SystemBarTintManager.SystemBarConfig config = systemBarTintManager.getConfig();
            findViewById(android.R.id.content).setPadding(0,
                    config.getPixelInsetTop(false),
                    0,
                    config.getPixelInsetBottom());
        }
    }

}
